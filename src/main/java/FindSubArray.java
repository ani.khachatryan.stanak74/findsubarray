class FindSubArray {
    public static String subArrayWithSum(int arr[], int searchedSum) {
        if (arr != null && arr.length > 0) {
            int sum = arr[0];
            int j = 0;

            for (int i = 1; i <= arr.length; i++) {
                while (sum > searchedSum && j < i - 1) {
                    sum -= arr[j];
                    j++;
                }

                if (sum == searchedSum) {
                    if (j == i - 1)
                        return "" + j;
                    else
                        return j + " to " + (i - 1);
                }

                if (i < arr.length)
                    sum += arr[i];

            }
        }

        return "Not found";
    }
}
