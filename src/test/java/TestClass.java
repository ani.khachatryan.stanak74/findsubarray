import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClass {
    @Test
    public void testWithEmptyArray() {
        int[] arr = {};
        String answer = FindSubArray.subArrayWithSum(arr, 22);
        assertEquals("Not found", answer);
    }

    @Test
    public void testWithNoMatchingSubArray() {
        int[] arr = {1,5,9,7};
        String answer = FindSubArray.subArrayWithSum(arr, 2);
        assertEquals("Not found", answer);
    }

    @Test
    public void testWithAnElementEqualToSearchedSum() {
        int[] arr = {1,5,7,9};
        String answer = FindSubArray.subArrayWithSum(arr, 5);
        assertEquals("1", answer);
    }

    @Test
    public void testWithMatchingSubArray() {
        int[] arr = {1,5,7,9};
        String answer = FindSubArray.subArrayWithSum(arr, 12);
        assertEquals("1 to 2", answer);
    }

    @Test
    public void testWithAnElementGreaterThanSearchedSumBeforeTheMatchingSubArray() {
        int[] arr = {1,9,2,3};
        String answer = FindSubArray.subArrayWithSum(arr, 5);
        assertEquals("2 to 3", answer);
    }

    @Test
    public void testWithArrayHavingSumOfAllElementsLessThanSearchedSum() {
        int[] arr = {1,9,2,3};
        String answer = FindSubArray.subArrayWithSum(arr, 30);
        assertEquals("Not found", answer);
    }

}

